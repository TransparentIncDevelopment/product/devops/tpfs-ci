# Dependency Update

This directory contains the core logic that our ci template uses to automatically update downstream projects.
All functionality is written in python in the `dependency_update/` subdirectory.

## Requirements

To work in this project, you must have Python 3.9+ and [Poetry 1.1.8+](https://python-poetry.org/docs/master/#installation) installed.

This is a Poetry project, so you can install the rest of the requirements by running (from `tpfs-ci/dependency_update/`):
```
poetry install
```

This project also depends on `rust-script`. You can install by running (or via other package managers):
```
cargo install rust-script
```

If you add any functionality that relies on additional third-party python libraries, please update `pyproject.toml` accordingly.
To do so, simply run (from `tpfs-ci/dependency_update/`):
```
poetry add <dependency_name>
```

## Registry update

The `update_registry_repo` method assumes that you have these three environment variables set:
```
NEW_BRANCH_NAME
```
to specify the branch name for your update MR
```
TPFS_GITLAB_CI_ACCESS_TOKEN
```
to specify the access token. CI should already have this variable set.
```
TPFS_GITLAB_CI_ACCESS_USERNAME
```
to specify the user who submits the MR. CI should already have this variable set.

## Testing

All unit tests and test artifacts live in the test folder `tests/`
To run all unit tests (from `tpfs-ci/dependency_update/`):
```
poetry run tests
```

All integration tests live in the test folder `tests_integ`. **Note: some of the integration tests require you to be logged
into GitLab so that they can interact with our repositories.**

Set these before running integ tests:
```

export TPFS_GITLAB_CI_ACCESS_TOKEN=<gitlab access token with full permissions> 
export TPFS_GITLAB_CI_ACCESS_USERNAME=gitlab-ci-token
echo -e "machine gitlab.com\nlogin gitlab-ci-token\npassword ${TPFS_GITLAB_CI_ACCESS_TOKEN}" > ~/.netrc
export NEW_BRANCH_NAME=<your_name>-test-123

```

To run all integration tests (from `tpfs-ci/dependency_update/`):
```
poetry run tests-integ
```
