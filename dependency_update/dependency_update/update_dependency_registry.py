import os

from dependency_update.constants import RELATIVE_PATH_TO_REGISTRY
from dependency_update.registry import read_registry, write_registry
from dependency_update.git_utils import pull_down_registry_repo, \
    checkout_new_branch, push_branch, merge, clean_up_repo


def update_registry_repo(consumer: str, deps: [str], allow_empty_mrs=False):
    repo, repo_path = pull_down_registry_repo()

    try:
        new_branch = checkout_new_branch(repo)
        registry_path = os.path.join(repo_path, RELATIVE_PATH_TO_REGISTRY)
        registry = read_registry(registry_path)
        if registry.update_deps(consumer, deps) or allow_empty_mrs:
            print("Updating registry!")
            write_registry(registry_path, registry)
            push_branch(repo, [consumer], new_branch, repo_path)
            merge(new_branch, [consumer])
        else:
            print("Nothing to update in registry. Skipping MR creation!")

    finally:
        clean_up_repo(repo_path)






