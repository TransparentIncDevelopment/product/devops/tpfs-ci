import os
import shutil
from time import sleep

from git import Repo

from dependency_update.constants import GITLAB_USER_NAME_ENV_VAR, GITLAB_TOKEN_ENV_VAR, \
    REGISTRY_REPO_URL_WITHOUT_PREFIX, CRATE_DEPENDENCY_REGISTRY_REPO_NAME, MASTER_BRANCH, NEW_BRANCH_NAME_ENV_VAR, \
    REGISTRY_REPO_PROJECT_ID, GITLAB_URL, RELATIVE_PATH_TO_REGISTRY
from dependency_update.gitlab_utils import (
    try_create_gitlab_connection,
)


def registry_repo_url(repo_url: str = REGISTRY_REPO_URL_WITHOUT_PREFIX) -> str:
    user_name = os.environ[GITLAB_USER_NAME_ENV_VAR]
    token = os.environ[GITLAB_TOKEN_ENV_VAR]
    return f"https://{user_name}:{token}@{repo_url}"


def pull_repo(repo_url, repo_name, branch) -> (Repo, str):
    cwd = os.getcwd()
    path_to_registry_repo = os.path.join(cwd, repo_name)
    repo = Repo.clone_from(repo_url, path_to_registry_repo, branch=branch)
    return repo, path_to_registry_repo


def pull_down_registry_repo() -> (Repo, str):
    url = registry_repo_url()
    repo, path = pull_repo(url, CRATE_DEPENDENCY_REGISTRY_REPO_NAME, MASTER_BRANCH)
    return repo, path


def checkout_new_branch(repo: Repo) -> str:
    new_branch_name = os.environ[NEW_BRANCH_NAME_ENV_VAR]
    new_branch = repo.create_head(new_branch_name)
    repo.head.reference = new_branch
    assert not repo.head.is_detached
    repo.head.reset(index=True, working_tree=True)
    return new_branch_name


def merge(new_branch: str, consumer_names: [str], project_id: str = REGISTRY_REPO_PROJECT_ID):
    print(f"Merging {new_branch} into {MASTER_BRANCH}")
    try:
        gl = try_create_gitlab_connection(GITLAB_URL, GITLAB_TOKEN_ENV_VAR)
        project = gl.projects.get(project_id)
        consumers = ', '.join(consumer_names)
        mr_params = {
            "source_branch": new_branch,
            "target_branch": MASTER_BRANCH,
            "title": f"Update entries for '{consumers}'",
            "labels": ["Team Cookie Monster"],
            "description": f"Updating entries for '{consumers}'",
        }
        mr = project.mergerequests.create(mr_params)
        mr.approvals.set_approvers(approvals_required=0)
        # Give MR time to be created before we try to merge
        sleep(10)

        retries = 100
        while True:
            try:
                mr.merge(merge_when_pipeline_succeeds=True, should_remove_source_branch=True)
                print("Successfully merged")
                return
            except:
                if retries > 0:
                    print("Failed to merge, retrying...")
                    retries -= 1
                    sleep(10)
                    continue
                else:
                    raise Exception("Failed to merge, exceeded retries.")
    except Exception as e:
        print("Failed to merge")
        print(e)


def remove_branch(branch_name: str, project_id: str = REGISTRY_REPO_PROJECT_ID):
    print(f"Removing branch {branch_name} from origin")
    gl = try_create_gitlab_connection(GITLAB_URL, GITLAB_TOKEN_ENV_VAR)
    project = gl.projects.get(project_id)
    project.branches.delete(branch_name)


def clean_up_repo(repo_path: str):
    shutil.rmtree(repo_path)


def push_branch(repo: Repo, consumers: [str], new_branch: str, repo_path: str):
    repo.git.add(update=True)
    repo.index.commit(f"Update entries for '{', '.join(consumers)}'")
    repo.git.push("--set-upstream", "origin", new_branch)
