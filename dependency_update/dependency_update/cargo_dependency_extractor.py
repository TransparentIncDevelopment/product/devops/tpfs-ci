import toml
from pathlib import Path
from functools import reduce
from itertools import chain
from typing import List
from dependency_update.constants import REGISTRY_FIELD, TPFS_REGISTRY_NAME, CARGO_TOML_FILENAME


class Dependency:

    def __init__(self, name: str, dependency_definition: any):
        self.name = name
        self.dependency_definition = dependency_definition

    def get_name(self) -> str:
        return self.name

    def is_tpfs(self) -> bool:
        return isinstance(self.dependency_definition, dict) \
            and REGISTRY_FIELD in self.dependency_definition \
            and self.dependency_definition[REGISTRY_FIELD] == TPFS_REGISTRY_NAME


def extract_dependencies_recursive(base_folder_path: str) -> List[str]:
    print(f"Recursively searching path '{base_folder_path}' for Cargo.toml files.")
    cargo_file_paths = list(Path(base_folder_path).rglob(CARGO_TOML_FILENAME))

    print(f"Found Cargo.toml files: %s"%(cargo_file_paths))
    local_crates = set(crate_names_from_paths(cargo_file_paths))

    print(f"Found local crates: %s"%(local_crates))
    all_dependencies = map(extract_dependencies_by_filepath, cargo_file_paths)
    flattened_dependencies = set(reduce(chain, all_dependencies, []))

    return list(sorted(flattened_dependencies.difference(local_crates)))


def crate_names_from_paths(cargo_file_paths: List[str]) -> List[str]:
    for cargo_dict in map(create_cargo_dict, cargo_file_paths):
        if "package" in cargo_dict:
            yield cargo_dict["package"]["name"]


def create_cargo_dict(cargo_toml_path: str) -> str:
    cargo_toml_file = open(cargo_toml_path, "r")
    file_content = cargo_toml_file.read()
    cargo_toml_file.close()

    return toml.loads(file_content)


def extract_dependencies_by_filepath(cargo_toml_path: str) -> List[str]:
    cargo_toml_file = open(cargo_toml_path, "r")
    file_content = cargo_toml_file.read()
    cargo_toml_file.close()

    dependencies = extract_dependencies(file_content)
    print(f"Extracted {len(dependencies)} tpfs dependencie(s) from Cargo file: '{cargo_toml_path}'")

    return dependencies


def extract_dependencies(cargo_toml_content: str) -> List[str]:
    cargo_toml = toml.loads(cargo_toml_content)
    cargo_dependencies = cargo_toml['dependencies'] if 'dependencies' in cargo_toml else []
    dependencies = map(lambda key: Dependency(key, cargo_dependencies[key]), cargo_dependencies)
    tpfs_dependencies = list(filter(Dependency.is_tpfs, dependencies))
    return list(map(Dependency.get_name, tpfs_dependencies))

