import os


def write_toml_file(file_path: str, contents: str) -> None:
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)

    with open(file_path, 'w') as f:
        f.write(contents)
        f.flush()
