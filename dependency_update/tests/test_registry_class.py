import unittest

from dependency_update.constants import CRATE_DEPENDENCIES_HEADER
from dependency_update.registry import Registry


def test_registry() -> Registry:
    inner = {"dep1": ["con1", "con2"], "dep2": ["con2", "con3"]}
    registry_toml = {CRATE_DEPENDENCIES_HEADER: inner}
    registry = Registry(registry_toml)
    return registry


class Tests(unittest.TestCase):
    def test_update_deps_adds_pair_to_existing_dep(self):
        registry = test_registry()
        update_consumer = "con3"
        update_deps = ["dep1", "dep2"]
        performs_update = registry.update_deps(update_consumer, update_deps)

        # Add con3 to dep1
        expected_inner = {"dep1": ["con1", "con2", "con3"], "dep2": ["con2", "con3"]}
        expected = {CRATE_DEPENDENCIES_HEADER: expected_inner}
        actual = registry.as_toml()

        assert(expected == actual)
        assert performs_update

    def test_update_deps_adds_new_dep_for_new_pair(self):
        registry = test_registry()
        update_consumer = "con3"
        update_deps = ["dep2", "dep3"]
        performs_update = registry.update_deps(update_consumer, update_deps)

        # Add new dep3 with con3
        expected_inner = {"dep1": ["con1", "con2"], "dep2": ["con2", "con3"], "dep3": ["con3"]}
        expected = {CRATE_DEPENDENCIES_HEADER: expected_inner}
        actual = registry.as_toml()

        assert(expected == actual)
        assert performs_update

    def test_update_deps_removes_old_pairs(self):
        registry = test_registry()
        update_consumer = "con2"
        update_deps = ["dep2"]
        performs_update = registry.update_deps(update_consumer, update_deps)

        # Remove con2 from dep1
        expected_inner = {"dep1": ["con1"], "dep2": ["con2", "con3"]}
        expected = {CRATE_DEPENDENCIES_HEADER: expected_inner}
        actual = registry.as_toml()

        assert(set(expected) == set(actual))
        assert performs_update

    def test_no_changes_keeps_parity(self):
        registry = test_registry()
        update_consumer = "con2"
        update_deps = ["dep1", "dep2"]
        performs_update = registry.update_deps(update_consumer, update_deps)

        # Remove con2 from dep1
        expected_inner = {"dep1": ["con1", "con2"], "dep2": ["con2", "con3"]}
        expected = {CRATE_DEPENDENCIES_HEADER: expected_inner}
        actual = registry.as_toml()

        assert(set(expected) == set(actual))
        assert not performs_update
