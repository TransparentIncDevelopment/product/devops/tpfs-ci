import tempfile
import unittest
import os
import tomli
# import utils

from dependency_update.update_strategies.cargo_update import CargoUpdate, _overwrite_dependencies, _get_workspace_members, _is_workspace
from utils import write_toml_file

NEW_VERSION="19.8.6"


class TestCargoUpdateStrategy(unittest.TestCase):
    def test__gets_members_from_workspace(self):
        base_toml_str = """
        [workspace]

        members = [
            "some-config-crate",
            "some-crate",
            "some-crate-server"
        ]
        """
        with tempfile.TemporaryDirectory() as temp_workspace_dir:
            workspace_cargo_file = os.path.join(temp_workspace_dir, "Cargo.toml")
            write_toml_file(workspace_cargo_file, base_toml_str)

            members = _get_workspace_members(temp_workspace_dir)

            assert members == ['some-config-crate', 'some-crate', 'some-crate-server']

    def test__overwrites_dep_version(self):
        base_toml_str = """
        [dependencies]
        url_serde = "0.2"
        uuid = { version = "0.7.4", features = ["v4"] }

        # 1st party local deps
        tpfs_logger_port = {version = "0.3", registry = "tpfs"}
        """
        with tempfile.TemporaryDirectory() as temp_dir:
            cargo_file = os.path.join(temp_dir, "Cargo.toml")
            write_toml_file(cargo_file, base_toml_str)

            _overwrite_dependencies(cargo_file, ["tpfs_logger_port"], NEW_VERSION)

            with open(cargo_file, "rb") as f:
                toml_dict = tomli.load(f)
                assert(toml_dict['dependencies']['tpfs_logger_port']['version'] == NEW_VERSION)

    def test__overwrites_dev_dep_version(self):
        base_toml_str = """
        [dev-dependencies]
        url_serde = "0.2"
        uuid = { version = "0.7.4", features = ["v4"] }

        # 1st party local deps
        tpfs_logger_port = {version = "0.3", registry = "tpfs"}
        """
        with tempfile.TemporaryDirectory() as temp_dir:
            cargo_file = os.path.join(temp_dir, "Cargo.toml")
            write_toml_file(cargo_file, base_toml_str)

            _overwrite_dependencies(cargo_file, ["tpfs_logger_port"], NEW_VERSION)

            with open(cargo_file, "rb") as f:
                toml_dict = tomli.load(f)
                assert(toml_dict['dev-dependencies']['tpfs_logger_port']['version'] == NEW_VERSION)

    def test__overwrites_build_dep_version(self):
        base_toml_str = """
        [build-dependencies]
        url_serde = "0.2"
        uuid = { version = "0.7.4", features = ["v4"] }

        # 1st party local deps
        tpfs_logger_port = {version = "0.3", registry = "tpfs"}
        """
        with tempfile.TemporaryDirectory() as temp_dir:
            cargo_file = os.path.join(temp_dir, "Cargo.toml")
            write_toml_file(cargo_file, base_toml_str)

            _overwrite_dependencies(cargo_file, ["tpfs_logger_port"], NEW_VERSION)

            with open(cargo_file, "rb") as f:
                toml_dict = tomli.load(f)
                assert(toml_dict['build-dependencies']['tpfs_logger_port']['version'] == NEW_VERSION)

    def test__detects_workspace_file(self):
        base_toml_str = """
        [workspace]

        members = [
            "some-config-crate",
        ]
        """
        with tempfile.TemporaryDirectory() as temp_workspace_dir:
            workspace_cargo_file = os.path.join(temp_workspace_dir, "Cargo.toml")
            write_toml_file(workspace_cargo_file, base_toml_str)

            assert(_is_workspace(temp_workspace_dir))

    def test__doesnt_misidentify_workspace(self):
        base_toml_str = """
        [dependencies]

        url_serde = "0.2"
        """
        with tempfile.TemporaryDirectory() as temp_workspace_dir:
            workspace_cargo_file = os.path.join(temp_workspace_dir, "Cargo.toml")
            write_toml_file(workspace_cargo_file, base_toml_str)

            assert(not _is_workspace(temp_workspace_dir))

    def test__updates_all_workspace_members(self):
        base_toml_str = """
        [workspace]

        members = [
            "awesomeco",
            "coolinc",
        ]
        """
        with tempfile.TemporaryDirectory() as temp_workspace_dir:
            workspace_cargo_file = os.path.join(temp_workspace_dir, "Cargo.toml")
            write_toml_file(workspace_cargo_file, base_toml_str)

            awesomeco_toml_str = """
            [dependencies]

            xand-banks = {version = "0.3", registry = "tpfs"}
            """
            awesomeco_cargo_file = os.path.join(temp_workspace_dir, "awesomeco", "Cargo.toml")
            write_toml_file(awesomeco_cargo_file, awesomeco_toml_str)

            coolinc_toml_str = """
            [dependencies]

            xand-banks = {version = "0.3", registry = "tpfs"}
            """
            coolinc_cargo_file = os.path.join(temp_workspace_dir, "coolinc", "Cargo.toml")
            write_toml_file(coolinc_cargo_file, coolinc_toml_str)

            update = CargoUpdate(["xand-banks"], NEW_VERSION)
            update.update_deps(temp_workspace_dir)

            with open(awesomeco_cargo_file, "rb") as f:
                toml_dict = tomli.load(f)
                assert(toml_dict['dependencies']['xand-banks']['version'] == NEW_VERSION)

            with open(coolinc_cargo_file, "rb") as f:
                toml_dict = tomli.load(f)
                assert(toml_dict['dependencies']['xand-banks']['version'] == NEW_VERSION)

    def test__updates_crate_dependencies(self):
        base_toml_str = """
        [dependencies]

        xand-banks = {version = "0.3", registry = "tpfs"}
        """
        with tempfile.TemporaryDirectory() as temp_crate_dir:
            cargo_file = os.path.join(temp_crate_dir, "Cargo.toml")
            write_toml_file(cargo_file, base_toml_str)

            update = CargoUpdate(["xand-banks"], NEW_VERSION)
            update.update_deps(temp_crate_dir)

            with open(cargo_file, "rb") as f:
                toml_dict = tomli.load(f)
                assert(toml_dict['dependencies']['xand-banks']['version'] == NEW_VERSION)

    def test__overwrite_dependencies_leaves_toml_format_intact(self):
        base_toml_str = """
# Some comment
[dependencies]
xand-banks = {version = "0.3", registry = "tpfs"}
"""
        with tempfile.TemporaryDirectory() as temp_crate_dir:
            cargo_file = os.path.join(temp_crate_dir, "Cargo.toml")
            write_toml_file(cargo_file, base_toml_str)

            update = CargoUpdate(["xand-banks"], "0.4")
            update.update_deps(temp_crate_dir)

            expected_file_contents = '''
# Some comment
[dependencies]
xand-banks = {version = "0.4", registry = "tpfs"}'''

            with open(cargo_file, "rb") as f:
                actual_file_contents = f.read().rstrip().decode('utf-8')
                self.assertMultiLineEqual(actual_file_contents,expected_file_contents)
                # assert(expected_file_contents == actual_file_contents)


