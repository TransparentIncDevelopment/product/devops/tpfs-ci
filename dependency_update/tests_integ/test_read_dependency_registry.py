import unittest
from dependency_update.read_dependency_registry import read_crate_consumers_from_remote_repo
from unittest.mock import patch
from dependency_update.read_dependency_registry import RegistryRepoConfig
from dependency_update.constants import RELATIVE_PATH_TO_REGISTRY, MASTER_BRANCH


class Tests(unittest.TestCase):
    def test_read_crate_dependencies_from_remote_repo__existing_crate(self):
        # Given a valid crate name and file path
        crate = "test-dependency"

        # When
        def repo_config_factory():
            return RegistryRepoConfig('https://gitlab.com/TransparentIncDevelopment/r-d/ci-integ-testing.git',
                'ci-integ-testing',
                RELATIVE_PATH_TO_REGISTRY,
                MASTER_BRANCH)
        res = read_crate_consumers_from_remote_repo(crate, repo_config_factory)

        # Then
        assert ('registry-update-test' in res)
