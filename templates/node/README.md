# tpfs npm Gitlib CI/CD Pipeline

Use this pipeline for projects containing local npm packages.  It will keep up to date packages published to artifactory for consumption in our JS/TS projects.  There should be no modifications necessary.  It follows the conventions laid out in the versioning proposal [here](https://gitlab.com/TransparentIncDevelopment/docs/engineering-guide/blob/versioning-proposal/versioning.md)

## Usage

Include the `npm-gitlab-ci.yaml` file as explained here: https://docs.gitlab.com/ee/ci/yaml/#includefile

```yaml
include:
    - project: 'transparentincdevelopment/product/devops/tpfs-ci'
      ref: master
      file: '/templates/node/gitlab-ci.yaml'
```

## Requirements

### package.json Versioning

The project's package.json versioning should be manually managed using semver with no prerelease tags specified as the pipeline will add them automatically.

### Branching

The pipeline assumes a `master` branch.  `master` does not allow pushes, only merge requests.

### npm Scripts

The following scripts are required by the pipeline:

- `test`
- `lint`
- `build`
    - Should build the project to a folder named `dist`

It also assumes that all dependencies are installed via `yarn install` during the `prepare` job.  If additional dependencies are needed, use [npm install hooks](https://docs.npmjs.com/misc/scripts#examples).

### Node Version

The image the pipeline uses is Node-lts, so the projects using it should support that version.

### Publish Config

Add the following to your `package.json`:

```js
  "publishConfig": {
    "registry": "https://transparentinc.jfrog.io/transparentinc/api/npm/npm/"
  },
```

This will ensure that npm publishes to our artifactory npm repository.

## Notes

### Caching and Artifacts

This pipeline uses caching set up in the `kubernetes-runner` during the `prepare` job to store `node_modules` and `yarn.lock`.  Other jobs will depend on this cache existing to correctly run and publish.  `dist` is published as an artifact that is used by the publish jobs and created by the `build` job.

### Publishing

#### beta

Any branch can publish a `beta` branch.  There is a `publish-beta` job that must be triggered manually in the gitlab pipeline UI.  When run, it will publish a package to artifactory with the prerelease identifier `-beta.CI-JOB-ID`.

#### release

To publish a release package, manually tag a specific commit with its matching release semver prepended by `v`.  This will trigger the pipeline for that commit and publish a release version to npm based on it.

The tag must match the format `vX.Y.Z` and the tag must match the version found in `package.json` for that commit:

```bash
git log # find hash of commit that you want to tag
git tag -a v{X.Y.Z} {commit-hash} -m "{release message}"
git push --tags
```

The pipeline to publish a release version will fail if the tag does not match the expected version.
