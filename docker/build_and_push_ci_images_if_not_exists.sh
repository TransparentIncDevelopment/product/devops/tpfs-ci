#!/usr/bin/env bash
# Note: This script is used in CI to conditionally build base docker images by comparing the tag
# specified in the neighboring `.env` file, and the tags published in dockerhub. If a matching tag already
# exists in dockerhub, this script will continue without building the image.


# Exit if error occurs
set -e

echo "Running script from: $(pwd)"

# Set /docker path by picking up location of this script
DOCKER_DIR="$( dirname "${BASH_SOURCE[0]}" )"

# Load build env vars and utility functions
source $DOCKER_DIR/.env
source $DOCKER_DIR/util.sh

# tpfs-rust img ##############################################################
echo "Working on" $TPFS_RUST_CI_IMG

TPFS_RUST_CI_IMG_EXISTS=$(does_manifest_exist "$TPFS_RUST_CI_IMG")

if [[ $TPFS_RUST_CI_IMG_EXISTS == "no" ]];
then
    # Print commands
    set -x
    docker build -t $TPFS_RUST_CI_IMG -f $DOCKER_DIR/rust/Dockerfile .
    docker push $TPFS_RUST_CI_IMG
    # Stop printing commands
    set +x
else
    echo "Image manifest for $TPFS_RUST_CI_IMG exists. Continuing..."
fi

# tpfs-node img ##############################################################
echo "Working on" $TPFS_NODE_CI_IMG

TPFS_NODE_CI_IMG_EXISTS=$(does_manifest_exist "$TPFS_NODE_CI_IMG")

if [[ $TPFS_NODE_CI_IMG_EXISTS == "no" ]];
then
    # Print commands
    set -x
    docker build -t $TPFS_NODE_CI_IMG -f $DOCKER_DIR/node/Dockerfile $DOCKER_DIR
    docker push $TPFS_NODE_CI_IMG
    # Stop printing commands
    set +x
else
    echo "Image manifest for $TPFS_NODE_CI_IMG exists. Continuing..."
fi

# tpfs-node:acceptance-runner img ##############################################################
echo "Working on" $TPFS_NODE_ACC_TEST_RUNNER_IMG

TPFS_NODE_ACC_TEST_RUNNER_IMG_EXISTS=$(does_manifest_exist "$TPFS_NODE_ACC_TEST_RUNNER_IMG")

if [[ $TPFS_NODE_ACC_TEST_RUNNER_IMG_EXISTS == "no" ]];
then
    # Print commands
    set -x
    docker build \
      -t $TPFS_NODE_ACC_TEST_RUNNER_IMG \
      -f $DOCKER_DIR/node/acceptance_test_runner/Dockerfile \
      --build-arg TPFS_NODE_CI_IMG=$TPFS_NODE_CI_IMG \
      $DOCKER_DIR
    docker push $TPFS_NODE_ACC_TEST_RUNNER_IMG
    # Stop printing commands
    set +x
else
    echo "Image manifest for $TPFS_NODE_ACC_TEST_RUNNER_IMG exists. Continuing..."
fi

# tpfs-python img ##############################################################
echo "Working on" $TPFS_PYTHON_CI_IMG

TPFS_PYTHON_CI_IMG_EXISTS=$(does_manifest_exist "$TPFS_PYTHON_CI_IMG")

if [[ $TPFS_PYTHON_CI_IMG_EXISTS == "no" ]];
then
    # Print commands
    set -x
    docker build -t $TPFS_PYTHON_CI_IMG -f $DOCKER_DIR/python/Dockerfile $DOCKER_DIR
    docker push $TPFS_PYTHON_CI_IMG
    # Stop printing commands
    set +x
else
    echo "Image manifest for $TPFS_PYTHON_CI_IMG exists. Continuing..."
fi

# tpfs-python:3.6 img ##############################################################
echo "Working on" $TPFS_PYTHON_3_6_CI_IMG

TPFS_PYTHON_3_6_CI_IMG_EXISTS=$(does_manifest_exist "$TPFS_PYTHON_3_6_CI_IMG")

if [[ $TPFS_PYTHON_3_6_CI_IMG_EXISTS == "no" ]];
then
    cp $DOCKER_DIR/python/Dockerfile $DOCKER_DIR/python/Dockerfile_3_6
    sed -i"" -e "s/:3-/:3.6-/g" $DOCKER_DIR/python/Dockerfile_3_6
    # Print commands
    set -x
    docker build -t $TPFS_PYTHON_3_6_CI_IMG -f $DOCKER_DIR/python/Dockerfile_3_6 $DOCKER_DIR
    docker push $TPFS_PYTHON_3_6_CI_IMG
    # Stop printing commands
    set +x
else
    echo "Image manifest for $TPFS_PYTHON_3_6_CI_IMG exists. Continuing..."
fi

# dockerhub img ##############################################################
echo "Working on" $TPFS_GCR_CI_IMG
TPFS_GCR_CI_IMG_EXISTS=$(does_manifest_exist "$TPFS_GCR_CI_IMG")

if [[ $TPFS_GCR_CI_IMG_EXISTS == "no" ]];
then
    # Print commands
    set -x
    docker build -t $TPFS_GCR_CI_IMG -f $DOCKER_DIR/gcr-ci/Dockerfile $DOCKER_DIR
    docker push $TPFS_GCR_CI_IMG
    # Stop printing commands
    set +x
else
    echo "Image manifest for $TPFS_GCR_CI_IMG exists. Continuing..."
fi
