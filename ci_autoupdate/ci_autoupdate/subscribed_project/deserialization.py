from pathlib import Path
from typing import Any, List, MutableMapping

import toml

from ci_autoupdate.constants import (
    ID_TOML_KEY,
    LABELS_TOML_KEY,
    NPM_PROJECT_TOML_KEY,
    RUST_PROJECT_TOML_KEY,
    SUBSCRIBED_PROJECT_EXPECTED_TOML_KEYS,
    SUBSCRIPTION_LIST_FILE,
)
from ci_autoupdate.subscribed_project.models import ProjectType, SubscribedProject
from ci_autoupdate.tool_logging import AutoUpdateEvent, AutoUpdateEventResult, AutoUpdateLogger


class SubscribedProjectDeserializeResult:
    def __init__(self, unrecognized_keys: List[str], missing_keys: List[str], project: SubscribedProject = None):
        self.unrecognized_keys = unrecognized_keys
        self.missing_keys = missing_keys
        self.project = project


def deserialize_subscribed_project_from_toml_dict(
    toml_dict: MutableMapping[str, Any], project_type: ProjectType
) -> SubscribedProjectDeserializeResult:
    actual_keys = set(toml_dict.keys())
    unrecognized_keys = actual_keys.difference(SUBSCRIBED_PROJECT_EXPECTED_TOML_KEYS)
    missing_keys = SUBSCRIBED_PROJECT_EXPECTED_TOML_KEYS.difference(actual_keys)
    if toml_dict.get(ID_TOML_KEY) == "":
        missing_keys.add(ID_TOML_KEY)
    if ID_TOML_KEY in missing_keys or LABELS_TOML_KEY in missing_keys:
        project = None
    else:
        project = SubscribedProject(toml_dict[ID_TOML_KEY], project_type, toml_dict[LABELS_TOML_KEY])
    return SubscribedProjectDeserializeResult(list(unrecognized_keys), list(missing_keys), project)


def deserialize_subscribed_projects(
    toml_dictionary: MutableMapping[str, Any], logger: AutoUpdateLogger = None
) -> List[SubscribedProject]:
    projects: List[SubscribedProject] = []
    rust_projects_dict: List[MutableMapping[str, Any]] = toml_dictionary.get(RUST_PROJECT_TOML_KEY, [])
    npm_projects_dict: List[MutableMapping[str, Any]] = toml_dictionary.get(NPM_PROJECT_TOML_KEY, [])
    for rust_project_toml in rust_projects_dict:
        log_deserialization_info(rust_project_toml, logger)
        rust_project_result = deserialize_subscribed_project_from_toml_dict(rust_project_toml, ProjectType.RUST)
        if rust_project_result.unrecognized_keys.__len__() > 0:
            log_deserialization_warn_unrecognized_keys(rust_project_result.unrecognized_keys, logger)
        if rust_project_result.project is not None:
            log_deserialization_success(rust_project_result.project, logger)
            projects.append(rust_project_result.project)
        else:
            log_deserialization_failure(rust_project_result, ProjectType.RUST, logger)

    for npm_project_toml in npm_projects_dict:
        log_deserialization_info(npm_project_toml, logger)
        npm_project_result = deserialize_subscribed_project_from_toml_dict(npm_project_toml, ProjectType.NPM)
        if npm_project_result.unrecognized_keys.__len__() > 0:
            log_deserialization_warn_unrecognized_keys(npm_project_result.unrecognized_keys, logger)
        if npm_project_result.project is not None:
            log_deserialization_success(npm_project_result.project, logger)
            projects.append(npm_project_result.project)
        else:
            log_deserialization_failure(npm_project_result, ProjectType.NPM, logger)
    return projects


def read_subscribed_projects_file(
    file_path: Path = SUBSCRIPTION_LIST_FILE, logger: AutoUpdateLogger = None
) -> MutableMapping[str, Any]:
    with open(file_path) as toml_file:
        toml_dict = toml.load(toml_file)
        if logger is not None:
            logger.log_info(
                f"Reading subscriptions toml at {str(file_path.absolute())}",
                AutoUpdateEvent.SUBSCRIPTION_PARSE,
            )
    return toml_dict


def log_deserialization_failure(
    result: SubscribedProjectDeserializeResult, project_type: ProjectType, logger: AutoUpdateLogger = None
):
    if logger is not None:
        logger.log_error(
            f"Could not parse {project_type.name} project!  This project will be skipped!  Missing keys: {str(result.missing_keys)}.  Unrecognized keys: {str(result.unrecognized_keys)}",  # noqa
            AutoUpdateEvent.SUBSCRIPTION_PARSE,
            AutoUpdateEventResult.FAILURE,
        )


def log_deserialization_success(project: SubscribedProject, logger: AutoUpdateLogger = None):
    if logger is not None:
        logger.log_info(
            f"Successfully parsed {project.project_type.name} {str(project.id)}.",
            AutoUpdateEvent.SUBSCRIPTION_PARSE,
            AutoUpdateEventResult.SUCCESS,
        )


def log_deserialization_warn_unrecognized_keys(keys: List[str], logger: AutoUpdateLogger = None):
    if logger is not None:
        logger.log_warning(
            f"Unrecognized keys!  {str(keys)}",
            AutoUpdateEvent.SUBSCRIPTION_PARSE,
            AutoUpdateEventResult.SUCCESS,
        )


def log_deserialization_info(toml_dict: MutableMapping[str, Any], logger: AutoUpdateLogger = None):
    if logger is not None:
        logger.log_debug(
            str(toml_dict),
            AutoUpdateEvent.SUBSCRIPTION_PARSE,
        )
