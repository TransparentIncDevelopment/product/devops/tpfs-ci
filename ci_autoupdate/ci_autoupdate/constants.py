from pathlib import Path
from typing import Final, Set

# Validation
TPFS_CI_PROJECT_ID: Final[int] = 16450823  # ID of https://gitlab.com/TransparentIncDevelopment/product/devops/tpfs-ci

# CLI Args
CLI_ARG_VERBOSE_SHORT: Final[str] = "v"
CLI_ARG_VERBOSE_LONG: Final[str] = "verbose"
CLI_ARG_POSITIONAL_NEW_VERSION: Final[str] = "newversion"

# Config Defaults
DEFAULT_BRANCH_PREFIX: Final[str] = ""
PROJECT_CLONE_DIR: Final[Path] = Path("temp_projects")
LOG_OUTPUT_DIR: Final[Path] = Path("logs")
SUBSCRIPTION_LIST_FILE: Final[Path] = Path("subscriptions/subscriptions.toml")
EXPECTED_TPFS_CI_IMPORT_FILE: Final[Path] = Path(".ci/tpfs.yaml")

# CI Import File YAML Keys
TPFS_CI_YAML_PARENT_KEY: Final[str] = "include"
TPFS_CI_YAML_VERSION_KEY: Final[str] = "ref"

# Subscribed Project TOML Keys
RUST_PROJECT_TOML_KEY: Final[str] = "rust"
NPM_PROJECT_TOML_KEY: Final[str] = "npm"
ID_TOML_KEY: Final[str] = "id"
LABELS_TOML_KEY: Final[str] = "labels"
SUBSCRIBED_PROJECT_EXPECTED_TOML_KEYS: Final[Set[str]] = set([ID_TOML_KEY, LABELS_TOML_KEY])

# Gitlab Actions
GITLAB_SERVER_URL: Final[str] = "https://gitlab.com"
GITLAB_ACCESS_TOKEN_ENV_VAR_NAME: Final[str] = "TPFS_GITLAB_CI_ACCESS_TOKEN"
GITLAB_ACCESS_TOKEN_USERNAMENAME_ENV_VAR_NAME: Final[str] = "TPFS_GITLAB_CI_ACCESS_USERNAME"
