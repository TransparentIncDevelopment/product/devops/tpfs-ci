"""
Tasks to be invoked via "poetry run".
Any task added here must also be specified as a "script" in pyproject.toml.
"""

import os
import subprocess
import sys
from typing import List, Union

from ci_autoupdate.cli_args import get_config
from ci_autoupdate.constants import CLI_ARG_VERBOSE_LONG
from ci_autoupdate.gitlab_actions import try_create_gitlab_connection
from ci_autoupdate.subscribed_project.deserialization import (
    deserialize_subscribed_projects,
    read_subscribed_projects_file,
)

repo_root = os.path.dirname(__file__)


def _command(command: Union[List[str], str], shell: bool = False):
    command_exit_code = subprocess.call(
        command,
        cwd=repo_root,
        shell=shell,
    )

    if command_exit_code != 0:
        sys.exit(command_exit_code)


def autoupdate():
    config = get_config()
    verbose_arg = ""
    if config.verbose:
        verbose_arg = "--" + CLI_ARG_VERBOSE_LONG
    command_array = [
        "python",
        "ci_autoupdate/run.py",
        config.new_version,
        verbose_arg,
    ]
    filtered_command_array = list(filter("".__ne__, command_array))
    _command(filtered_command_array)


def get_subscribers():
    toml_dict = read_subscribed_projects_file()
    subscriptions = deserialize_subscribed_projects(toml_dict)
    connection = try_create_gitlab_connection()
    if connection is not None:
        for project in subscriptions:
            gitlab_project = connection.projects.get(project.id)
            print(f"{str(project.id)}: {gitlab_project.name} - {gitlab_project.web_url}")


def update_snapshots():
    _command(["pytest", "--snapshot-update"])


def test():
    _command(["pytest", "tests/", "-vv"])


def pytest_watch():
    _command(["pytest-watch", "tests/"])


def test_integ():
    _command(["pytest", "tests_integration/", "-vv"])


def test_validation():
    _command(["pytest", "tests_validation/", "-vv"])


def lint():
    _command(["flakehell", "lint"])


def format():
    _command(["black", "."])
    _command(["isort", "."])


def typecheck():
    # shell=True because pyright installs as scripts "pyright.cmd" and "pyright.ps1" on Windows.
    # Passing through CMD will let it implicitly add the ".cmd".
    _command(["pyright"], shell=True)
