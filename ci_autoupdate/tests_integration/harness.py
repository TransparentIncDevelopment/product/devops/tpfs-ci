import shutil
from datetime import datetime
from pathlib import Path
from typing import Any, Final, MutableMapping

import pytest
from gitlab import Gitlab  # type: ignore

from ci_autoupdate.git_actions import try_clone_project
from ci_autoupdate.gitlab_actions import try_create_gitlab_connection
from ci_autoupdate.subscribed_project.deserialization import (
    deserialize_subscribed_projects,
    read_subscribed_projects_file,
)
from ci_autoupdate.subscribed_project.models import SubscribedProject

INTEG_TEST_CLONE_DIRECTORY: Final[Path] = Path("test_clone_directory")
INTEG_TEST_RUST_SUBSCRIPTION_FILE_PATH: Final[Path] = Path("tests_integration/rust_integ_subscription.toml")
INTEG_TEST_NPM_SUBSCRIPTION_FILE_PATH: Final[Path] = Path("tests_integration/npm_integ_subscription.toml")


class TestData:
    def __init__(
        self, connection: Gitlab, project: SubscribedProject, local_path: Path, test_branch_name_prefix: str
    ) -> None:
        self.connection = connection
        self.project = project
        self.local_path = local_path
        # -autoupdate is automatically appended to any supplied prefix during tool run
        self.test_branch_name_prefix = test_branch_name_prefix


# Tells pytest this is not a class containing test methods
TestData.__test__ = False  # type: ignore


def setup_cloned_rust_project_for_test() -> TestData:
    toml_dict = read_subscribed_projects_file(INTEG_TEST_RUST_SUBSCRIPTION_FILE_PATH)
    return setup_cloned_project_for_test(toml_dict)


def setup_cloned_npm_project_for_test() -> TestData:
    toml_dict = read_subscribed_projects_file(INTEG_TEST_NPM_SUBSCRIPTION_FILE_PATH)
    return setup_cloned_project_for_test(toml_dict)


def setup_cloned_project_for_test(toml_dict: MutableMapping[str, Any]) -> TestData:
    subscriptions = deserialize_subscribed_projects(toml_dict)
    connection = try_create_gitlab_connection()
    assert connection is not None  # Make typechecker happy
    directory = INTEG_TEST_CLONE_DIRECTORY
    rust_project = subscriptions[0]
    clone_result = try_clone_project(connection, rust_project.id, directory)
    assert clone_result is not None

    test_branch_name = f'integ-test-{datetime.now().strftime("%Y-%m-%dT%H-%M-%S")}'

    data = TestData(connection, rust_project, clone_result.local_path, test_branch_name)
    return data


@pytest.fixture(autouse=False)
def cloned_rust_project_harness():
    """
    SO reference: https://stackoverflow.com/a/22638709/14290955
    This snippet will run for all tests in this module that reference
    this fixture in their signature
    """

    # Code that will run before your test,
    print("\nTest setup")
    test_data: TestData = setup_cloned_rust_project_for_test()

    # A test function will be run at this point
    yield test_data

    # Code that will run after test
    print("Test cleanup")
    project_id = test_data.project.id
    project = test_data.connection.projects.get(project_id)
    project.branches.delete(test_data.test_branch_name_prefix + "-autoupdate")
    shutil.rmtree(INTEG_TEST_CLONE_DIRECTORY)


@pytest.fixture(autouse=False)
def cloned_npm_project_harness():
    """
    SO reference: https://stackoverflow.com/a/22638709/14290955
    This snippet will run for all tests in this module that reference
    this fixture in their signature
    """

    # Code that will run before your test,
    print("\nTest setup")
    test_data: TestData = setup_cloned_npm_project_for_test()

    # A test function will be run at this point
    yield test_data

    # Code that will run after test
    print("Test cleanup")
    project_id = test_data.project.id
    project = test_data.connection.projects.get(project_id)
    project.branches.delete(test_data.test_branch_name_prefix + "-autoupdate")
