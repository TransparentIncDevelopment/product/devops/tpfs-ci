#!/usr/bin/env bash

set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

function helptext {
HELPTEXT=$(cat <<END
    This script is intended to be run by a scheduled GitLab CI job. It cleans up stale caches on the sticky-vm.

    Arguments
        BUILD_ROOT (Required) = Static root directory for all build environments on the sticky-vm
        MAX_AGE_IN_DAYS (Required) = Any branch+repo contexts with files newer than this threshold won't be cleaned up
END
)
echo "$HELPTEXT"
}

function error {
    echo $1
    echo
    echo "$(helptext)"
    exit 1
}

#No Arguments
if [[ $# -eq 0 ]] ; then
    error "Missing arguments"
fi

BUILD_ROOT=${1:?"$(error 'BUILD_ROOT must be set' )"}
MAX_AGE_IN_DAYS=${2:?"$(error 'MAX_AGE_IN_DAYS must be set' )"}

DIGITS_RE='^[0-9]+$'
if ! [[ "${MAX_AGE_IN_DAYS}" =~ $DIGITS_RE ]]; then
    error "$MAX_AGE_IN_DAYS must be a number"
fi


function has_fresh_files {
    local dir="$1"
    local max_age_in_days="$2"
    local fresh_file_count=$(find "$dir" -type f -mtime "-${max_age_in_days}" | wc -l)

    if [[ "$fresh_file_count" -gt 0 ]]
    then
        return 0
    else
        return 1
    fi
}

function main {
    local build_dir="$1"
    local max_age_in_days="$2"

    for runner_dir in "${build_dir}"/*
    do
        for branch_dir in "${runner_dir}"/*
        do
            # skip the cargohome directory
            if (echo "${branch_dir}" | grep "cargohome$")
            then
                echo "Skipping the CARGO_HOME directory ..."
                continue
            fi

            for repo_dir in "${branch_dir}"/*
            do
                if has_fresh_files "${repo_dir}" "${max_age_in_days}"
                then
                    echo "${repo_dir} is fresh ..."
                else
                    echo "${repo_dir} is stale; deleting ..."
		    rm -rf "${repo_dir}"
                fi
            done
        done
    done
}


BYTES_FREE_START=$(df --output=avail / | tail -n 1)
main "${BUILD_ROOT}" "${MAX_AGE_IN_DAYS}"
echo "Cleaned up $(expr $(df --output=avail / | tail -n 1) - $BYTES_FREE_START) bytes"
